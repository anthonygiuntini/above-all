#Above All
#Keeps Images on Top

import os
import os.path
import platform
import requests
import webbrowser
import traceback, sys, time
from shutil import copyfile, copytree

from tkinter import *
from tkinter import filedialog, messagebox
from PIL import Image, ImageTk, ImageOps

__VERSION__ = '1.0.2' #beta

SYSTEM = platform.system() #'Darwin' for mac, 'Windows' for windows, 'Linux' for linux
if hasattr(sys, 'frozen'):
    CWD = os.path.split(sys.executable)[0]
else:
    CWD = os.getcwd()
print(CWD)
#CWD = os.getcwd()

if SYSTEM == 'Windows': #Windows
    DATADIR = os.path.join(os.getenv("APPDATA"), "aboveall")
else: #Mac/linux
    DATADIR = os.path.join(os.getenv("HOME"), ".aboveall")

IMGDIR = os.path.join(DATADIR, "images")

#First time make DATADIR
if not os.path.exists(IMGDIR):
    if not os.path.exists(DATADIR):
        os.makedirs(DATADIR)
    copytree(os.path.join(CWD,'AboveAllImages'),IMGDIR)
ICONPATH = os.path.join(DATADIR,'AboveAllIcon.png')
if not os.path.exists(ICONPATH):
    copyfile(os.path.join(CWD,'AboveAllIcon.png'),ICONPATH)


class MainWindow(Frame):

    def __init__(self, master):
        '''
        The main window with the image and intention. This always stays on top.
        '''
        Frame.__init__(self, master)
        self.master = master
        self.master.configure(bg='black')
        self.bind("<Configure>", self.on_resize)
        self.master.bind('<Left>', self.on_left_arrow)
        self.master.bind('<Right>', self.on_right_arrow)
        #sets the icon for windows only
        if SYSTEM == 'Windows':
            self.master.iconbitmap('icon.ico')

        self.intention_on = BooleanVar()
        self.intention_on.set(True)


        self.timer_value = DoubleVar()
        self.timer_value.set(0)
        self.timer = None

        self.width = 175
        self.height = 250
        self.master.geometry(str(self.width)+"x"+str(self.height))

        self.move_window_after_time()

        #Set starting image
        self.refresh_images()
        self.index = 0

        self.cimagepath = os.path.join(IMGDIR,self.image_list[self.index])
        pil_image = self.load_and_resize(self.cimagepath)

        if pil_image is not None:
            self.cimage = ImageTk.PhotoImage(pil_image)
        else:
            #This ignores the .ds_store file
            self.index += 1
            self.cimagepath = os.path.join(IMGDIR,self.image_list[self.index])
            pil_image = self.load_and_resize(self.cimagepath)

            self.cimage = ImageTk.PhotoImage(pil_image)

        #set initial image
        self.image_label = Label(self,image=self.cimage, bg='black')
        self.image_label.image = self.cimage
        self.image_label.pack()

        self.intentionvar = StringVar()
        self.intentionvar.set("Click to edit intention")
        self.intention_label = Label(self, textvariable=self.intentionvar, bg='black', fg='white', justify=CENTER)

        self.intention_label.bind("<Button-1>", self.change_intention)

        if self.intention_on.get():
            self.intention_label.pack(fill=X, side=BOTTOM)


        #self.set_image(self.cimagepath)
        self.master.wm_title("Above All")
        #p = os.path.join(os.getenv("HOME"), ".floatingimages", "data.txt")

    def load_and_resize(self,path, size=None):
        '''
        Opens and resizes the given image, returns the image obj. Default size is current
        screen size. size should be a tuple (width,height).

        '''
        accepted_exts = ['.jpg','.png','.jpeg']
        if os.path.splitext(path)[1].lower() in accepted_exts:
            pil_image = Image.open(path)

            width, height = pil_image.size
            ratio = width/height

            if size is None:
                set_width, set_height =(int(self.height*ratio),self.height)
            else:
                set_width, set_height = (int(size[1]*ratio),size[1])

            modified_image = pil_image.resize((set_width,set_height),Image.ANTIALIAS)

            return modified_image
        else:
            return None

    def set_image(self, path):
        '''
        Sets the image that is displayed.
        '''
        #clear previous image
        self.image_label.pack_forget()

        pil_image = self.load_and_resize(path)
        if pil_image is not None:

        #modified_image = ImageOps.fit(pil_image, (250,250))
        #modified_image = pil_image.resize((self.width,int(self.height*1/ratio)))

            self.cimage = ImageTk.PhotoImage(image=pil_image)

            self.image_label.configure(image=self.cimage, bg='black')
            self.image_label.image = self.cimage
            self.image_label.pack()


    def open_image(self):
        '''
        Allows user to add new image.
        '''

        #open file dialog
        filepath = filedialog.askopenfilename(filetypes = (("jpeg files","*.jpg"),("png","*.png")))

        try:
            assert os.path.exists(filepath)

            #copy file to DATADIR
            newpath = os.path.join(IMGDIR,os.path.split(filepath)[-1])
            loaded_img = self.load_and_resize(filepath,size=(500,500)) #500px so images are clearer
            if loaded_img is not None:
                loaded_img.save(newpath,optimize=True,quality=95) #Image compression

                self.cimagepath = newpath

                self.set_image(newpath)

        except AssertionError:
            print("MainWindow.open_image: AssertionError")

    def del_image(self):
        '''
        Allows user to select previously added image.
        '''
        self.top = Toplevel()
        self.top.geometry("+500+200") #place window at 500x200
        DeleteImagesScreen(self.top,self).pack()

    def on_resize(self,event):
        '''
        Called when the window is resized.
        '''
        self.width = event.width
        self.height = event.height

        self.set_image(self.cimagepath)

        #This solves problem where the label is clicked when resizing
        try:
            self.enter_intention(None)
        except AttributeError:
            pass
    def on_right_arrow(self, event):
        '''
        Called when right arrow key is clicked.
        '''
        self.image_list = os.listdir(IMGDIR)
        if self.index<len(self.image_list)-1:
            self.index +=1
        else:
            self.index = 0
        self.cimagepath = os.path.join(IMGDIR,self.image_list[self.index])
        self.set_image(self.cimagepath)

    def on_left_arrow(self, event):
        '''
        Called when right arrow key is clicked.
        '''
        self.image_list = os.listdir(IMGDIR)
        if self.index>0:
            self.index-=1
        else:
            self.index = len(self.image_list)-1
        self.cimagepath = os.path.join(IMGDIR,self.image_list[self.index])
        self.set_image(self.cimagepath)

    def refresh_images(self):
        '''
        Refreshes the image list.
        '''
        self.image_list = os.listdir(IMGDIR)

    def change_intention(self,event):
        '''Replaces intention label with entry for new intention. '''
        self.intention_label.pack_forget()
        self.intention_entry = Entry(self, textvariable=self.intentionvar, bg="black", fg="white")
        self.intention_entry.pack(fill=X, side=BOTTOM)
        #Must repack the image
        self.set_image(self.cimagepath)
        self.master.bind("<Return>", self.enter_intention)

    def enter_intention(self,event):
        '''
        Accepts input from intention entry, then replaces label.
        '''
        self.intention_entry.pack_forget()
        self.intention_label.pack(fill=X, side=BOTTOM)
        #Must repack the image
        self.set_image(self.cimagepath)

    def toggle_intention(self):
        '''
        Toggles the intention label on or off.
        '''
        if self.intention_on.get():
            self.intention_label.pack(fill=X, side=BOTTOM)
            self.set_image(self.cimagepath)
        else:
            self.intention_label.pack_forget()

    def move_window_after_time(self):
        '''This the timer, it moves the window after a certain time.
        '''
        #Get center of screen
        x = self.master.winfo_screenwidth()//2 - self.width//2
        y = self.master.winfo_screenheight()//2 - self.height//2

        if self.timer_value.get():
            self.master.geometry("+"+str(x)+"+"+str(y))
            if self.timer is not None:
                self.master.after_cancel(self.timer)
            self.timer = self.master.after(self.convert_time(self.timer_value.get()), self.move_window_after_time)

    def convert_time(self, minutes):
        '''
        converts minutes to ms.
        '''
        return int(minutes * 60000)


class DeleteImagesScreen(Frame):
    def __init__(self,master, main_screen_i):
        '''
        Screen that shows the previously downloaded images.
        '''

        self.main_screen_i = main_screen_i

        Frame.__init__(self,master)
        self.master.title('Loaded Images')
        if SYSTEM == 'Windows':
            self.master.iconbitmap('icon.ico')
        self.master.resizable(False, False)
        self.config(bg='black')
        self.img_listbox = Listbox(self.master, height=10, width=35, selectmode='SINGLE')
        self.img_listbox.pack(padx=10,pady=5)

        set_button = Button(self.master, text='Remove Image', command=self.del_img, width=10)
        set_button.pack(ipadx=5)

        self.update_lb()

    def update_lb(self):
        '''Updates the listbox'''
        self.img_listbox.delete(0,END)
        self.current_img = os.listdir(IMGDIR)
        for path in self.current_img:
            self.img_listbox.insert(END,os.path.split(path)[-1])

    def del_img(self):
        imgpath = os.path.join(IMGDIR,self.img_listbox.get(ACTIVE))
        assert os.path.exists(imgpath)

        os.remove(imgpath)
        self.main_screen_i.refresh_images()

        newpath = os.path.join(IMGDIR,self.main_screen_i.image_list[0])
        self.main_screen_i.set_image(newpath)

        self.update_lb()

    def exit(self):
        '''Closes the window'''
        self.master.destroy()


def config_menubar(menubar, main_window):
    '''
    Configures the menu bar.
    '''
    filemenu = Menu(menubar, tearoff=0)
    filemenu.add_command(label="Add Image", command=main_window.open_image)
    filemenu.add_command(label="Remove Image", command=main_window.del_image)

    timermenu = Menu(menubar, tearoff=0)
    timermenu.add_radiobutton(label="Off", value=0, variable=main_window.timer_value)
    #timermenu.add_radiobutton(label="30 Seconds", value=.5, command=main_window.move_window_after_time, variable=main_window.timer_value)
    timermenu.add_radiobutton(label="1 Minute", value=1, command=main_window.move_window_after_time, variable=main_window.timer_value)
    timermenu.add_radiobutton(label="10 Minutes", value=10, command=main_window.move_window_after_time, variable=main_window.timer_value)
    timermenu.add_radiobutton(label="15 Minutes", value=15, command=main_window.move_window_after_time, variable=main_window.timer_value)
    timermenu.add_radiobutton(label="30 Minutes", value=30, command=main_window.move_window_after_time, variable=main_window.timer_value)
    timermenu.add_radiobutton(label="45 Minutes", value=45, command=main_window.move_window_after_time, variable=main_window.timer_value)
    timermenu.add_radiobutton(label="50 Minutes", value=50, command=main_window.move_window_after_time, variable=main_window.timer_value)
    timermenu.add_radiobutton(label="1 Hour", value=60, command=main_window.move_window_after_time, variable=main_window.timer_value)
    configmenu = Menu(menubar, tearoff=0)
    configmenu.add_checkbutton(label="Intention Caption", command=main_window.toggle_intention, onvalue=True, offvalue=False, variable=main_window.intention_on)
    configmenu.add_cascade(label="Timer", menu=timermenu)

    menubar.add_cascade(label="File", menu=filemenu)
    menubar.add_cascade(label="Config", menu=configmenu)

class UpdatePopup(Frame):
    def __init__(self,master, root, version_str):
        '''
        Popup that notifies user of a new software version.
        '''

        self.master = master
        self.root = root        
        Frame.__init__(self,master)
        self.master.title('Above All Version '+ version_str + ' is Available')
        if SYSTEM == 'Windows':
            self.master.iconbitmap('icon.ico')
        self.config(width=35)

        #Get icon
        pil_image = Image.open(ICONPATH)
        modified_image = pil_image.resize((60,60),Image.ANTIALIAS)
        img = ImageTk.PhotoImage(modified_image)

        b_frame = Frame(self)
        b_frame.pack(anchor='s',side=RIGHT)

        image_label = Label(self,image=img)
        image_label.image = img
        image_label.pack(anchor='n',side=LEFT,padx=5,pady=5)

        label_text = ('There is a new version of The Above All App!'
        '\nPlease go to The Above All App website to download it.')
        Label(b_frame, text = label_text).pack(anchor='n',side=TOP,pady=5)


        site_button = Button(b_frame, text='Go to website', command=self.go_to_site, width=10)
        site_button.pack(pady=5,padx=5, ipadx=5, side=RIGHT, anchor='s')

        close_button = Button(b_frame, text='Close', command=self.master.destroy, width=10)
        close_button.pack(pady=5,padx=5, ipadx=5, side=RIGHT, anchor='s')

    def go_to_site(self):
        webbrowser.open('https://theaboveallapp.github.io/#Download')
        self.master.destroy()
        self.root.destroy()

def check_for_updates(master):
    '''Checks for updates, then alerts user if there is an update available.'''
    url = 'https://bitbucket.org/anthonygiuntini/above-all/wiki/Home.md'
    r = requests.get(url)
    if r.status_code == 200:
        response_txt = r.text
    else:
        response_txt = None
        print("Update Check Failed")
    bracket_str = 'current-version' #tag is in format "bracket_strversion#bracket_str"
    bracket_str_len = len(bracket_str)
    if response_txt is not None:
        a = response_txt.find(bracket_str)
        if a != -1: #find returns -1 if nothing found
            b = response_txt.find(bracket_str,a+bracket_str_len)
            if b != -1:
                web_ver = response_txt[a+bracket_str_len:b]
                if web_ver != __VERSION__:
                    print("New Version Available")
                    top = Toplevel()
                    top.geometry('+300+100')
                    UpdatePopup(top,master,web_ver).pack()
                    #messagebox.showinfo('Above All Version '+web_ver, 'There is a new version of the Above All App! Go to theaboveallapp.github.io to download the latest version.')

    print(web_ver)
    print('Current Version: %s\nOnline Version: %s' %(__VERSION__,web_ver))
    #print(str_ver)

if __name__ == '__main__':
    root = Tk()
    mw = MainWindow(root)

    menubar = Menu(root)
    config_menubar(menubar, mw)
    emptymenu = Menu(root)
    root.config(menu=menubar)
    # def show_menu(event):
    #     root.config(menu=menubar)
    # def hide_menu(event):
    #         root.config(menu=emptymenu)
    # hide_menu(None)
    #FUTURE: hide taskbar for windows
    #root.bind('<Enter>', show_menu)
    #menubar.bind('<Enter>', show_menu)
    #root.bind('<Leave>', hide_menu)


    mw.pack()
    #delay update check so screen loads first
    root.after(10000,lambda: check_for_updates(root))

    #This keeps the window on top
    root.call('wm', 'attributes', '.', '-topmost', '1')
    #This sets the icon
    root.mainloop()
