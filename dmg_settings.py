#Settings for dmgbuild application

#Must match current version name
#To build dmg: dmgbuild -s dmg_settings.py "Above All" AboveAll.dmg
APPNAME = 'Above All 1.0.2 (beta).app'
APPPATH = 'build/' + APPNAME
VERSION = '1.0.2'
filename = 'Above All ' + VERSION + '.dmg'

volume_name = 'Above All (installer)'

format = 'UDZO'

size = '150m'

files = [APPPATH]

symlinks = {'Applications':'/Applications'}

icon = 'icon.icns'

icon_locations = {
    APPNAME: (100, 300),
    'Applications': (400, 300)
}

background = 'AboveAllLogo_small.png'

default_view = 'icon-view'

window_rect = ((150,150),(480,460))
