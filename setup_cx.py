#cx_Freeze setup script
#to build do python setup_cx.py build
import os, sys
import platform
from cx_Freeze import setup, Executable

SYSTEM = platform.system()
__version__ = '1.0.2'
if SYSTEM == 'Windows':
    os.environ['TCL_LIBRARY'] = r'C:\Users\giunt\AppData\Local\Programs\Python\Python36-32\tcl\tcl8.6'
    os.environ['TK_LIBRARY'] = r'C:\Users\giunt\AppData\Local\Programs\Python\Python36-32\tcl\tk8.6'
    icon = "icon.ico"
    includefiles=[icon,r"C:\Users\giunt\AppData\Local\Programs\Python\Python36-32\DLLs\tcl86t.dll", r"C:\Users\giunt\AppData\Local\Programs\Python\Python36-32\DLLs\tk86t.dll","AboveAllImages"]


elif SYSTEM == 'Darwin':
    icon = "icon.icns"
    includefiles=[icon, "AboveAllImages",'AboveAllIcon.png']
else:
    includefiles = []

base = None
if sys.platform == "win32":
    base = "Win32Gui"

exe=Executable(
     script="Above All.py",
     base=base,
     icon=icon,
     shortcutName="Above All (beta)",
     shortcutDir="DesktopFolder"
     )

includes=['tkinter']
excludes=[]
packages=['tkinter','PIL','requests','webbrowser','idna']
setup(

     version = __version__,
     description = "An app that helps you focus on what is important while working on your computer.",
     author = "Above All Devs",
     name = "Above All %s (beta)" % __version__,
     options = {'build_exe': {'excludes':excludes,'packages':packages,'include_files':includefiles,},
     'bdist_mac':{'iconfile':'icon.icns', 'bundle_name':'Above All %s (beta)' % __version__},
     'bdist_dmg':{'volume_label':'Above All-%s beta' % __version__,'applications_shortcut':True},
     'bdist_msi':{'add_to_path':False,'initial_target_dir':r'[ProgramFilesFolder]\Above All','upgrade_code':'{c6441021-0730-4cff-a7b9-eb0b5476bccf}'}
     },
     executables = [exe]
     )
